package br.com.itau.PontoEletronico.services;

import br.com.itau.PontoEletronico.DTOs.FolhaDePontoDTO;
import br.com.itau.PontoEletronico.enums.TipoBatidaEnum;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.repositories.BatidaPontoRepository;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
public class BatidaPontoService {

    @Autowired
    private BatidaPontoRepository batidaPontoRepository;

    @Autowired
    private UsuariosService usuariosService;


    //INCLUSÃO
    public BatidaPonto incluirBatidaPonto(BatidaPonto batidaPonto){

        BatidaPonto objBatidaPonto = batidaPontoRepository.save(batidaPonto);

        return objBatidaPonto;
    }


    //CONSULTAS
    public Iterable<BatidaPonto> consultarTodasBatidas(){

        Iterable<BatidaPonto> objPonto = batidaPontoRepository.findAll();

        return objPonto;
    }



    public FolhaDePontoDTO consultarPontoPorUsuario(int id){
//    public Iterable<BatidaPonto> consultarPontoPorUsuario(int id){

        Usuarios objUsuario = usuariosService.consultarUsuarioPorId(id);

        if(objUsuario == null){

            throw new RuntimeException("Usuario não encontrado!");
        }else{
            Iterable<BatidaPonto> objPonto = batidaPontoRepository.findAllByUsuario(objUsuario);
//            Iterable<BatidaPonto> objPonto = batidaPontoRepository.findByUsuario_id(id);

            ArrayList<BatidaPonto> lstPonto = new ArrayList<>();
            lstPonto = (ArrayList<BatidaPonto>) objPonto;

            DateTimeFormatter dFormat = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
            DateTime dataEntrada = new DateTime();
            DateTime dataSaida = new DateTime();
            long qtdHorasEmMinutos = 0;

//            double xxxx = 0.0;

            for(BatidaPonto pontox : objPonto){

                if(pontox.getTipoBatida() == TipoBatidaEnum.ENTRADA){
                    dataEntrada = DateTime.parse(pontox.getDataHoraBatida(), dFormat);
//                    System.out.println("dataEnt: " + dataEntrada);

                }else {
                    dataSaida = DateTime.parse(pontox.getDataHoraBatida(), dFormat);
//                    System.out.println("dataSai: " + dataSaida);
                }
                if(dataEntrada != null && dataSaida != null){

//                    xxxx += Minutes.minutesBetween(dataEntrada, dataSaida).getMinutes();
//                    System.out.println("MINUTS: " + xxxx);

                    Duration duracao = new Duration(dataEntrada, dataSaida);
                    qtdHorasEmMinutos += duracao.getStandardMinutes();

//                    System.out.println("qtdxxx: " + qtdHorasEmMinutos);
                    dataEntrada = null;
                    dataSaida = null;
                }
            }
            FolhaDePontoDTO respostaConsulta = new FolhaDePontoDTO();
            respostaConsulta.setNomeCompleto(objUsuario.getNomeCompleto());
            respostaConsulta.setLstPontos(lstPonto);
            respostaConsulta.setTotalHorasTrabalhadas((double) qtdHorasEmMinutos / 60);
//            respostaConsulta.setTotalHorasTrabalhadas(xxxx / 60);

        return respostaConsulta;
//        return objPontox;
        }
    }
}

//    public Iterable<BatidaPonto> consultarPontoPorUsuariox(int id){
//
//        Usuarios objUsuariox = usuariosService.consultarUsuarioPorId(id);
//
//        Iterable<BatidaPonto> objPontox = batidaPontoRepository.findByUsuario_id(id);
//
//        ArrayList<BatidaPonto> lstPontox = new ArrayList<>();
//        lstPontox = (ArrayList<BatidaPonto>) objPontox;
//
//        DateTimeFormatter dformat = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
//        DateTime dataHoraAnteriorFormat = DateTime.parse(lstPontox.get(0).getDataHoraBatida(), dformat);
////        int qtdHoras = 0;
////        int qtdTotalHoras = 0;
////        int qtdMin = 0;
////        int qtdMinTot = 0;
//        double qtdSeg = 0.0;
//        double qtdSeqTot = 0.0;
//
//        System.out.println("DataInicialFormat: " + dataHoraAnteriorFormat);
//        System.out.println("");
//
//
//
//        for(int x=0; x < lstPontox.size(); x++){
//
//            DateTime dataHoraAtualFormat = DateTime.parse(lstPontox.get(x).getDataHoraBatida(), dformat);
//
//            System.out.println("DataAnterior: " + dataHoraAnteriorFormat);
//            System.out.println("DataAtual: " + dataHoraAtualFormat);
//
////            qtdHoras = qtdHoras + Hours.hoursBetween(dataHoraAnteriorFormat, dataHoraAtualFormat).getHours();
////            qtdMin = qtdMin + Minutes.minutesBetween(dataHoraAnteriorFormat, dataHoraAtualFormat).getMinutes();
//            qtdSeg = qtdSeg + Seconds.secondsBetween(dataHoraAnteriorFormat,dataHoraAtualFormat).getSeconds();
//
//
//            dataHoraAnteriorFormat = dataHoraAtualFormat;
////            qtdTotalHoras = qtdTotalHoras + qtdHoras;
////            qtdMinTot = qtdMinTot + qtdMin;
//            qtdSeqTot = qtdSeqTot + qtdSeg;
//
////            System.out.println("qtdhorasTotal: " + qtdTotalHoras);
////            System.out.println("qtdMinTot: " + qtdMinTot);
//            System.out.println("qtdSegTot: " + qtdSeqTot);
//            System.out.println("TotalHoras: " + qtdSeqTot / 3600);
//            System.out.println("--------------");
//
//        }
//
//
//        FolhaDePontoDTO respostaConsulta = new FolhaDePontoDTO();
//        respostaConsulta.setNomeCompleto(objUsuariox.getNomeCompleto());
//        respostaConsulta.setLstPontos(lstPontox);
//        respostaConsulta.setTotalHorasTrabalhadas(10);
//
//        return objPontox;
//    }
