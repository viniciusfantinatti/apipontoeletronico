package br.com.itau.PontoEletronico.controllers;

import br.com.itau.PontoEletronico.DTOs.FolhaDePontoDTO;
import br.com.itau.PontoEletronico.DTOs.UsuarioDTO;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.services.BatidaPontoService;
import br.com.itau.PontoEletronico.services.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/usuario")
public class UsuariosController {

    @Autowired
    private UsuariosService usuariosService;

    @Autowired
    private BatidaPontoService batidaPontoService;



    //INCLUSÃO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuarios cadastrarUsuarios(@RequestBody @Valid Usuarios usuario){

        Usuarios objUsuarios = usuariosService.cadastrarUsuario(usuario);

        return objUsuarios;
    }


    //ALTERAÇÃO
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Usuarios alterarUsuario(@PathVariable @Valid int id, @RequestBody @Valid UsuarioDTO usuarioDTO){

        try{

            Usuarios objUsuario = usuariosService.alterarUsuarios(id, usuarioDTO);

            return objUsuario;

        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    //CONSULTAS
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Usuarios> consultarUsuarios(){

        Iterable<Usuarios> objUsuarios = usuariosService.consultarTodosUsuarios();

        return objUsuarios;
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Usuarios consultarUsuarioPorId(@PathVariable(name = "id") int id){

        try {

            Usuarios objUsuario = usuariosService.consultarUsuarioPorId(id);

            return objUsuario;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }


    //INCLUSÃO PONTO POR USUARIO
    @PostMapping("/{id}/ponto")
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaPonto baterPonto(@PathVariable(name = "id") int id,
                                  @RequestBody  @Valid BatidaPonto batidaPonto){

        BatidaPonto objPonto = usuariosService.baterPonto(id, batidaPonto);

        return objPonto;
    }

    //CONSULTA TODOS PONTOS DO USUARIO
    @GetMapping("/{id}/ponto")
    @ResponseStatus(HttpStatus.OK)
    public FolhaDePontoDTO consultarPontoPorUsuario(@PathVariable(name = "id") int id){

        try{
            FolhaDePontoDTO objFolhaPonto = batidaPontoService.consultarPontoPorUsuario(id);

            return objFolhaPonto;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
}
