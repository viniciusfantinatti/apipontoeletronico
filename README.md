API Ponto Eletronico

##### CADASTRAR USUARIO #####
POST: http://localhost:8080/usuario

Body:
{
    "nomeCompleto": "Maria Julia da Silca",
    "cpf": "29684356803",
    "email": "maria@email.com",
    "dataCadastro": "11/05/2011"
}

Body Retorno:
{
    "id": 1,
    "nomeCompleto": "Maria Julia da Silca",
    "cpf": "29684356803",
    "email": "maria@email.com",
    "dataCadastro": "11/05/2011"
}

StatusCode: 201 Created


##### ALTERAR USUARIO #####
PUT: http://localhost:8080/usuario/{id}

Body:
{
    "nomeCompleto": "Maria Julia da Silva",
    "cpf": "29684356803",
    "email": "mariajulia@email.com"
}

Body Retorno:
{
    "id": 1,
    "nomeCompleto": "Maria Julia da Silva",
    "cpf": "29684356803",
    "email": "mariajulia@email.com",
    "dataCadastro": "11/05/2011"
}

StatusCode: 200 OK

##### CONSULTAR USUARIO #####
GET: http://localhost:8080/usuario/{id}

Body Retorno:
{
    "id": 1,
    "nomeCompleto": "Maria Julia da Silva",
    "cpf": "29684356803",
    "email": "mariajulia@email.com",
    "dataCadastro": "11/05/2011"
}

StatusCode: 200 OK

##### LISTAR TODOS USUARIOS #####
GET: http://localhost:8080/usuario

Body Retorno:
[
    {
        "id": 1,
        "nomeCompleto": "Maria Julia da Silva",
        "cpf": "29684356803",
        "email": "mariajulia@email.com",
        "dataCadastro": "11/05/2011"
    },
    {
        "id": 2,
        "nomeCompleto": "Roberto Pereira",
        "cpf": "48292771069",
        "email": "robertopereira@email.com",
        "dataCadastro": "01/05/1991"
    }
]

StatusCode: 200 OK

##### BATER PONTO #####
POST: http://localhost:8080/usuario/1/ponto

Body:
{
    "tipoBatida": "ENTRADA"
}

Body Retorno:
{
    "id": 1,
    "dataHoraBatida": "05/07/2020 17:56",
    "tipoBatida": "ENTRADA",
    "usuario": {
        "id": 1,
        "nomeCompleto": "Maria Julia da Silva",
        "cpf": "29684356803",
        "email": "mariajulia@email.com",
        "dataCadastro": "11/05/2011"
    }
}

StatusCode: 201 Created

##### LISTAR TODOS OS PONTOS #####
GET: http://localhost:8080/ponto

Body Retorno:
[
    {
        "id": 1,
        "dataHoraBatida": "05/07/2020 17:56",
        "tipoBatida": "ENTRADA",
        "usuario": {
            "id": 1,
            "nomeCompleto": "Maria Julia da Silva",
            "cpf": "29684356803",
            "email": "mariajulia@email.com",
            "dataCadastro": "11/05/2011"
        }
    },
    {
        "id": 2,
        "dataHoraBatida": "05/07/2020 17:59",
        "tipoBatida": "ENTRADA",
        "usuario": {
            "id": 2,
            "nomeCompleto": "Roberto Pereira",
            "cpf": "48292771069",
            "email": "robertopereira@email.com",
            "dataCadastro": "01/05/1991"
        }
    },
    {
        "id": 3,
        "dataHoraBatida": "05/07/2020 19:46",
        "tipoBatida": "SAIDA",
        "usuario": {
            "id": 1,
            "nomeCompleto": "Maria Julia da Silva",
            "cpf": "29684356803",
            "email": "mariajulia@email.com",
            "dataCadastro": "11/05/2011"
        }
    }
]

StatusCode: 200 OK

##### CONSULTAR TODAS BATIDAS DE UM USUARIO #####
GET: http://localhost:8080/usuario/3/ponto

Body Retorno:
{
    "nomeCompleto": "Vinicius Fantinatti",
    "totalHorasTrabalhadas": 0.25,
    "lstPontos": [
        {
            "id": 10,
            "dataHoraBatida": "05/07/2020 21:19",
            "tipoBatida": "ENTRADA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 11,
            "dataHoraBatida": "05/07/2020 21:22",
            "tipoBatida": "SAIDA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 13,
            "dataHoraBatida": "05/07/2020 22:39",
            "tipoBatida": "ENTRADA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 14,
            "dataHoraBatida": "05/07/2020 22:40",
            "tipoBatida": "SAIDA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 15,
            "dataHoraBatida": "05/07/2020 22:41",
            "tipoBatida": "ENTRADA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 16,
            "dataHoraBatida": "05/07/2020 22:42",
            "tipoBatida": "SAIDA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 17,
            "dataHoraBatida": "05/07/2020 23:05",
            "tipoBatida": "ENTRADA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        },
        {
            "id": 18,
            "dataHoraBatida": "05/07/2020 23:15",
            "tipoBatida": "SAIDA",
            "usuario": {
                "id": 3,
                "nomeCompleto": "Vinicius Fantinatti",
                "cpf": "48292771069",
                "email": "robertopereira@email.com",
                "dataCadastro": "01/05/1991"
            }
        }
    ]
}

StatusCode: 200 OK