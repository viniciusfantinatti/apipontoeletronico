package br.com.itau.PontoEletronico.repositories;

import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BatidaPontoRepository extends CrudRepository<BatidaPonto, Integer> {

    Iterable<BatidaPonto> findAllByUsuario(Usuarios usuarios);

    @Query(value = "SELECT * FROM batida_ponto WHERE usuario_id = ?1 ORDER BY data_hora_batida", nativeQuery = true)
    Iterable<BatidaPonto> findByUsuario_id(Integer id);
}
