package br.com.itau.PontoEletronico.services;

import br.com.itau.PontoEletronico.DTOs.FolhaDePontoDTO;
import br.com.itau.PontoEletronico.enums.TipoBatidaEnum;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.repositories.BatidaPontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootTest
public class BatidaPontoServiceTest {

    @MockBean
    private BatidaPontoRepository batidaPontoRepository;

    @MockBean
    private UsuariosService usuariosService;

    @Autowired
    private BatidaPontoService batidaPontoService;


    Usuarios usuario;
    BatidaPonto ponto;

    @BeforeEach
    public void inicializar(){

        usuario = new Usuarios();
        ponto = new BatidaPonto();

        usuario.setId(1);
        usuario.setNomeCompleto("José Maria Souza Silva");
        usuario.setCpf("71961249014");
        usuario.setEmail("zemariasouzasilva@email.com");
        usuario.setDataCadastro("01/01/2001");

        ponto.setId(1);
        ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);
        ponto.setDataHoraBatida(LocalDateTime.now().toString());
        ponto.setUsuario(usuario);
    }

    @Test
    public void testarIncluirPonto_Positivo(){

        Mockito.when(batidaPontoRepository.save(Mockito.any(BatidaPonto.class))).thenReturn(ponto);

        BatidaPonto objPonto = batidaPontoService.incluirBatidaPonto(ponto);

        Assertions.assertSame(ponto, objPonto);
    }

    @Test
    public void testarConsultarTodasBatidas_Positivo(){

        Iterable<BatidaPonto> arrPonto = Arrays.asList(ponto);

        Mockito.when(batidaPontoRepository.findAll()).thenReturn(arrPonto);

        Iterable<BatidaPonto> objPonto = batidaPontoService.consultarTodasBatidas();

        Assertions.assertSame(arrPonto, objPonto);
    }

//    @Test
//    public void testarConsultarPontoPorUsuario_Positivo(){
//
//        Iterable<BatidaPonto> folhaPonto = Arrays.asList(ponto);
//
//        Mockito.when(usuariosService.consultarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);
//        Mockito.when(batidaPontoRepository.findByUsuario_id(Mockito.anyInt())).thenReturn(folhaPonto);
//
//        FolhaDePontoDTO objFolhaPonto = batidaPontoService.consultarPontoPorUsuario(ponto.getUsuario().getId());
//
//        Assertions.assertSame(folhaPonto, objFolhaPonto);
//    }
}
