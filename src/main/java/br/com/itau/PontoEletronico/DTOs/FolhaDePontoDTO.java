package br.com.itau.PontoEletronico.DTOs;

import br.com.itau.PontoEletronico.models.BatidaPonto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class FolhaDePontoDTO {

    private String nomeCompleto;
    private double totalHorasTrabalhadas;
    private ArrayList<BatidaPonto> lstPontos;


    public FolhaDePontoDTO() {
    }

    public FolhaDePontoDTO(String nomeCompleto, double totalHorasTrabalhadas, ArrayList<BatidaPonto> lstPontos) {
        this.nomeCompleto = nomeCompleto;
        this.totalHorasTrabalhadas = totalHorasTrabalhadas;
        this.lstPontos = lstPontos;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public ArrayList<BatidaPonto> getLstPontos() {
        return lstPontos;
    }

    public void setLstPontos(ArrayList<BatidaPonto> lstPontos) {
        this.lstPontos = lstPontos;
    }

    public double getTotalHorasTrabalhadas() {
        return totalHorasTrabalhadas;
    }

    public void setTotalHorasTrabalhadas(double totalHorasTrabalhadas) {
        BigDecimal bigDecTotHoras = new BigDecimal(totalHorasTrabalhadas).setScale(2,RoundingMode.FLOOR);
        this.totalHorasTrabalhadas = bigDecTotHoras.doubleValue();
    }
}
