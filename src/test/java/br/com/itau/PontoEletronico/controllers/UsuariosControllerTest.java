package br.com.itau.PontoEletronico.controllers;

import br.com.itau.PontoEletronico.DTOs.UsuarioDTO;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.repositories.UsuariosRepository;
import br.com.itau.PontoEletronico.services.BatidaPontoService;
import br.com.itau.PontoEletronico.services.UsuariosService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

@WebMvcTest(UsuariosController.class)
public class UsuariosControllerTest {

    @MockBean
    private UsuariosService usuariosService;

    @MockBean
    private UsuariosRepository usuariosRepository;

    @MockBean
    private BatidaPontoService batidaPontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuarios usuario, usuario2;
    UsuarioDTO usuarioDTO;
    BatidaPonto ponto;

    @BeforeEach
    public void inicializar(){

        usuario = new Usuarios();
        usuarioDTO = new UsuarioDTO();
        ponto = new BatidaPonto();

        usuario.setNomeCompleto("José Maria Souza Silva");
        usuario.setCpf("71961249014");
        usuario.setEmail("zemariasouzasilva@email.com");
        usuario.setDataCadastro("01/01/2001");

        usuarioDTO.setNomeCompleto("Maria Joaquina souza");
        usuarioDTO.setCpf("41108269001");
        usuarioDTO.setEmail("mariquinha@email.com");

//        ponto.setId(1);
//        ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);
//        ponto.setDataHoraBatida(LocalDateTime.now().toString());
//        ponto.setUsuario(usuario);
    }

    @Test
    public void testarCadastrarUsuario() throws Exception{

        usuario.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        Mockito.when(usuariosService.cadastrarUsuario(Mockito.any(Usuarios.class))).thenReturn(usuario);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.nomeCompleto",CoreMatchers.equalTo(usuario.getNomeCompleto())))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.cpf", CoreMatchers.equalTo(usuario.getCpf())))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.email", CoreMatchers.equalTo(usuario.getEmail())))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.dataCadastro", CoreMatchers.equalTo(usuario.getDataCadastro())));
    }

    @Test
    public void testarAlterarUsuario_Positivo() throws Exception {

        usuario.setId(1);
        usuario2 = usuario;
        usuario2.setCpf(usuarioDTO.getCpf());
        usuario2.setEmail(usuarioDTO.getEmail());
        usuario2.setNomeCompleto(usuarioDTO.getNomeCompleto());

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuarioDTO = mapper.writeValueAsString(usuarioDTO);

        Mockito.when(usuariosService.consultarUsuarioPorId(Mockito.anyInt()))
                .thenReturn(usuario);
        Mockito.when(usuariosService.alterarUsuarios(Mockito.anyInt(), Mockito.any(UsuarioDTO.class)))
                .thenReturn(usuario2);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuarioDTO))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(jsonUsuarioDTO));
    }
}
