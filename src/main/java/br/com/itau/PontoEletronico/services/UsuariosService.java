package br.com.itau.PontoEletronico.services;


import br.com.itau.PontoEletronico.DTOs.UsuarioDTO;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.repositories.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Autowired
    private BatidaPontoService batidaPontoService;


    public Usuarios cadastrarUsuario(Usuarios usuario){

        Usuarios objUsuario = usuariosRepository.save(usuario);

        return objUsuario;
    }


    public Usuarios alterarUsuarios(int id, UsuarioDTO usuarioDTO){

        Usuarios usuario = consultarUsuarioPorId(id);

        usuario.setNomeCompleto(usuarioDTO.getNomeCompleto());
        usuario.setCpf(usuarioDTO.getCpf());
        usuario.setEmail(usuarioDTO.getEmail());

        Usuarios objUsuarios = usuariosRepository.save(usuario);

        return objUsuarios;
    }


    public Iterable<Usuarios> consultarTodosUsuarios(){

        Iterable<Usuarios> objUsuarios = usuariosRepository.findAll();

        return objUsuarios;
    }


    public Usuarios consultarUsuarioPorId(int id){

        Optional<Usuarios> optUsuarios = usuariosRepository.findById(id);

        if(optUsuarios.isPresent()){

            return optUsuarios.get();
        }else {

            throw new RuntimeException("Usuario não encontrado.");
        }
    }


    public BatidaPonto baterPonto(int id, BatidaPonto batidaPonto){

        Optional<Usuarios> optUsuarios  = usuariosRepository.findById(id);

        if (!optUsuarios.isPresent()){
            throw new RuntimeException("Usuario não encontrado.");
        }

        batidaPonto.setUsuario(optUsuarios.get());
        batidaPonto.setDataHoraBatida(pegarDataHora());

        BatidaPonto objBatidaPonto = batidaPontoService.incluirBatidaPonto(batidaPonto);

        return objBatidaPonto;
    }

    private String pegarDataHora() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

}