package br.com.itau.PontoEletronico.models;

import br.com.itau.PontoEletronico.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;


@Entity
@JsonIgnoreProperties(value = {"dataHoraBatida"}, allowGetters = true)
public class BatidaPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private int id;

    @DateTimeFormat(pattern ="dd/MM/yyyy HH:mm")
    private String dataHoraBatida;


    private TipoBatidaEnum tipoBatida;

    @ManyToOne(cascade = CascadeType.ALL)
    private Usuarios usuario;

    public BatidaPonto() {
    }

    public BatidaPonto(int id, String dataHoraBatida, TipoBatidaEnum tipoBatida, Usuarios usuario) {
        this.id = id;
        this.dataHoraBatida = dataHoraBatida;
        this.tipoBatida = tipoBatida;
        this.usuario = usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(String dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }
}
