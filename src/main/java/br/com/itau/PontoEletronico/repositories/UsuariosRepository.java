package br.com.itau.PontoEletronico.repositories;

import br.com.itau.PontoEletronico.models.Usuarios;
import org.springframework.data.repository.CrudRepository;

public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {
}
