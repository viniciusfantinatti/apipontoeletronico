package br.com.itau.PontoEletronico.services;


import br.com.itau.PontoEletronico.DTOs.UsuarioDTO;
import br.com.itau.PontoEletronico.enums.TipoBatidaEnum;
import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.models.Usuarios;
import br.com.itau.PontoEletronico.repositories.UsuariosRepository;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuariosServiceTest {

    @MockBean
    private UsuariosRepository usuariosRepository;

    @MockBean
    private BatidaPontoService batidaPontoService;

    @Autowired
    private UsuariosService usuariosService;


    Usuarios usuario, usuario2;
    UsuarioDTO usuarioDTO;
    BatidaPonto ponto;


    @BeforeEach
    public void inicializar(){

        usuario = new Usuarios();
        usuario2 = new Usuarios();
        usuarioDTO = new UsuarioDTO();
        ponto = new BatidaPonto();

        usuario.setId(1);
        usuario.setNomeCompleto("José Maria Souza Silva");
        usuario.setCpf("71961249014");
        usuario.setEmail("zemariasouzasilva@email.com");
        usuario.setDataCadastro("01/01/2001");

        usuarioDTO.setNomeCompleto("Maria Joaquina souza");
        usuarioDTO.setCpf("41108269001");
        usuarioDTO.setEmail("mariquinha@email.com");


        ponto.setId(1);
        ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);
        ponto.setDataHoraBatida(LocalDateTime.now().toString());
        ponto.setUsuario(usuario);
    }

    @Test
    public void testarCadastrarUsuario_Positivo(){

        Mockito.when(usuariosRepository.save(Mockito.any(Usuarios.class))).thenReturn(usuario);

        Usuarios objUsuario = usuariosService.cadastrarUsuario(usuario);

        Assertions.assertSame(usuario, objUsuario);
    }

    @Test
    public void testarAlterarUsuario_Positivo(){

        Mockito.when(usuariosRepository.save(Mockito.any(Usuarios.class))).thenReturn(usuario);
        Mockito.when(usuariosRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));

        Usuarios objUsuario = usuariosService.alterarUsuarios(usuario.getId(), usuarioDTO);

        Assertions.assertSame(usuario, objUsuario);
    }

    @Test
    public void testarConsultarTodosUsuarios_Positivo(){

        Iterable<Usuarios> arrUsuarios = Arrays.asList(usuario);
        Mockito.when(usuariosRepository.findAll()).thenReturn(arrUsuarios);

        Iterable<Usuarios> objUsuarios = usuariosService.consultarTodosUsuarios();

        Assertions.assertSame(arrUsuarios,objUsuarios);
    }

    @Test
    public void testarConsultarUsuarioPorID_Positivo(){

        Mockito.when(usuariosRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));

        Usuarios objUsuario = usuariosService.consultarUsuarioPorId(usuario.getId());

        Assertions.assertSame(usuario, objUsuario);
    }

    @Test
    public void testarBaterPonto_Positivo(){

        Mockito.when(usuariosRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Mockito.when(batidaPontoService.incluirBatidaPonto(Mockito.any(BatidaPonto.class))).thenReturn(ponto);

        BatidaPonto objBatidaPonto = usuariosService.baterPonto(usuario.getId(), ponto);

        Assertions.assertSame(ponto, objBatidaPonto);
    }


    @Test
    public void testarConsultarUsuarioPorID_Negativo(){

        Mockito.when(usuariosRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class, () -> {usuariosService.consultarUsuarioPorId(2);});
    }

}
