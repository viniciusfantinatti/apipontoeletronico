package br.com.itau.PontoEletronico.controllers;

import br.com.itau.PontoEletronico.models.BatidaPonto;
import br.com.itau.PontoEletronico.services.BatidaPontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/ponto")
public class BatidaPontoController {

    @Autowired
    private BatidaPontoService batidaPontoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaPonto incluirPonto(@RequestBody @Valid BatidaPonto batidaPonto){
        BatidaPonto objBatidaPonto = batidaPontoService.incluirBatidaPonto(batidaPonto);

        return objBatidaPonto;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<BatidaPonto> consultarTodasBatidaPonto(){
        Iterable<BatidaPonto> objBatidaPonto = batidaPontoService.consultarTodasBatidas();

        return objBatidaPonto;
    }

}
